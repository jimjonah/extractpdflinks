package com.jimjonah.pdf;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class ExtractPdfLinks {

	public static void main(String[] args) throws IOException {
		Document doc = Jsoup.connect("https://www.waynecounty.com/elected/commission/fc-archived-journals.aspx").get();
		Elements links = doc.select("a[href]");
		for (Element link : links) {
			String pdfUrl = link.attr("abs:href");
			if (pdfUrl.contains(".pdf") && pdfUrl.contains("commission")) {
				System.out.println(pdfUrl);
				
				int idx = pdfUrl.lastIndexOf('/');
				String pdfName = pdfUrl.substring(idx+1);
				System.out.println(pdfName);
				
				FileUtils.copyURLToFile(
						  new URL(pdfUrl), 
						  new File(pdfName), 
						  5000, 
						  5000);
				
//				break;
			}
		}
	}

}
